package main

import (
	"bytes"
	"strings"
	"text/template"
	"strconv"
	"observer_adapter/xlog"
	"encoding/json"
)

const (
	RESOURCE_TYPE_DEFAULT = "DEFAULT"
	RESOURCE_TYPE_CLUSTER = "CLUSTER"
	RESOURCE_TYPE_NODE = "NODE"
	RESOURCE_TYPE_APP = "APP"
	RESOURCE_TYPE_SERVICE = "SERVICE"
	RESOURCE_TYPE_COMPONENT = "COMPONENT"
	RESOURCE_TYPE_CONTAINER = "CONTAINER"
	RESOURCE_KEY_CLUSTER = "cluster_name"
	RESOURCE_KEY_HOST = "host"
	RESOURCE_KEY_KUBE_NAMESPACE = "kube_namespace"
	RESOURCE_KEY_APP = "app_name"
	RESOURCE_KEY_SERVICE = "service_name"
)

var (
	resourceClusterKeys = []string{"cluster_name"}
	resourceNodeKeys = []string{"cluster_name", "host"}
	resourceServiceKeys = []string{"cluster_name", "kube_namespace", "service_name"}
	resourceAppKeys = []string{"cluster_name", "kube_namespace", "app_name"}
	syslogLanguage string
)


type NotificationAlarm struct {
	Name string `json:"alarm_name"`
	Status string `json:"alarm_status"`
	Value JsonFloat64 `json:"current_value,omitempty"`
	Start string `json:"start_time"`
	Comparison string `json:"comparison"`
	Threshold float64 `json:"threshold"`
	Interval int `json:"notification_interval"`
	Severity string `json:"serverity_level,omitempty"`
	StatsTsdb string `json:"statistic"`
	StatsDuration int `json:"statistic_duration"`
	MetricName string `json:"metric_name"`
	MetricNameDis string `json:"metric_display_name,omitempty"`
	MetricUnit string `json:"metric_unit,omitempty"`
	ResourceName string `json:"resource_name,omitempty"`
}

type MapResource map[string]string

type JsonFloat64 struct {
	Valid bool `json:"-"`
	Set bool `json:"-"`
	Value float64
}

func init() {
	syslogLanguage = strings.ToUpper(getenv("LANGUAGE", "en"))
}

func (m *JsonFloat64) UnmarshalJSON(data []byte) error {
	// If this method was called, the value was set.
	m.Set = true
	if string(data) == "null" {
		// The key was set to null
		m.Valid = false
		return nil
	}
	// The key isn't set to null
	var temp string
	if err := json.Unmarshal(data, &temp); err != nil {
		return err
	}
	m.Value, _ = strconv.ParseFloat(temp, 64)
	m.Valid = true
	return nil
}

func (m JsonFloat64) MarshalJSON() ([]byte, error) {
	if m.Set == false {
		return []byte("null"), nil
	}
	numStr, err := json.Marshal(strconv.FormatFloat(m.Value, 'f', -1, 64))
	return numStr, err
}

func (m MapResource) isExist(keys []string) bool {
	for _, key := range keys {
		if _, ok := m[key]; !ok {
			return false
		}
	}
	return true
}

func (m MapResource) toResourceMsg(keys []string) string {
	msg := bytes.NewBufferString("")
	for _, key := range keys {
		msg.WriteString(m[key])
		msg.WriteString(",")
	}
	return strings.Trim(msg.String(), ",")
}

func (m MapResource) set(resourceName string) {
	if resourceName != "" {
		resourceSlice := strings.Split(resourceName, ",")
		for _, values := range resourceSlice {
			keyValue := strings.Split(values, "=")
			m[keyValue[0]] = keyValue[1]
		}
	}	
}

func (n *NotificationAlarm) setSeverity() {
	severity := n.Severity
	if n.Status == "ALARM" || n.Status == "警报" {
		n.Severity = "CRITICAL"
	} else if n.Status == "INSUFFICIENT_DATA" || n.Status == "数据不足" {
		n.Severity = "ALERT"
	} else if n.Status == "WARNING_ALARM" || n.Status == "警告警报" {
		n.Severity = "WARNING"
	} else {
		n.Severity = "INFO"
	}

	if n.Severity == "CRITICAL" || n.Severity == "WARNING" {
		if severity != "" {
			if severity == "major" {
				n.Severity = "CRITICAL"
			} else if severity == "minor" {
				n.Severity = "WARNING"
			}
		}	
	}
}

func (n *NotificationAlarm) isComponent(resourceMap MapResource) bool {
	var status bool
	if strings.HasPrefix(n.MetricName, "comp") {
		status = true
	} else if strings.HasPrefix(n.MetricName, "docker") {
		if value, ok := resourceMap["container_type"]; ok {
			if value == "component" {
				status = true
			}
		}
		if _, ok := resourceMap["comp_name"]; ok {
			status = true
		}
	}
	return status
}

func (n *NotificationAlarm) isService(resourceMap MapResource) bool {
	var status bool
	if strings.HasPrefix(n.MetricName, "service") {
		status = true
	} else if strings.HasPrefix(n.MetricName, "docker") {
		if value, ok := resourceMap["container_type"]; ok {
			if value == "service" {
				status = true
			}
		}
		if _, ok := resourceMap["service_name"]; ok {
			status = true
		}
	}
	return status
}

func (n *NotificationAlarm) isContainer(resourceMap MapResource) bool {
	var status bool
	if strings.HasPrefix(n.MetricName, "service") || strings.HasPrefix(n.MetricName, "comp") || strings.HasPrefix(n.MetricName, "docker") {
		status = true
	}
	return status
}

func (n *NotificationAlarm) isNode() bool {
	var status bool
	if strings.HasPrefix(n.MetricName, "node") {
		status = true
	}
	return status
}

func (n *NotificationAlarm) isCluster() bool {
	var status bool
	if strings.HasPrefix(n.MetricName, "cluster") || strings.HasPrefix(n.MetricName, "region") {
		status = true
	}
	return status
}

func (n *NotificationAlarm) getResourceType () string {
	var resourceType string
	resourceMap := make(MapResource)
	resourceMap.set(n.ResourceName)
	if n.isComponent(resourceMap) {
		resourceType = RESOURCE_TYPE_COMPONENT
	} else if n.isService(resourceMap) {
		resourceType = RESOURCE_TYPE_SERVICE
	} else if n.isContainer(resourceMap) {
		resourceType = RESOURCE_TYPE_CONTAINER
	} else if n.isNode() {
		resourceType = RESOURCE_TYPE_NODE
	} else if n.isCluster() {
		resourceType = RESOURCE_TYPE_CLUSTER
	} else {
		resourceType = RESOURCE_TYPE_DEFAULT
	}
	return resourceType
}

func (n *NotificationAlarm) getResourceMsg () string {
	var resourceMsg string
	if n.ResourceName != "" {
		resourceMsg = strings.Replace(n.ResourceName, "=", "@", -1)
		resourceMsg = strings.Replace(resourceMsg, ",", "|", -1)
	} else {
		resourceMsg = "*"
	}
	return resourceMsg
}

func (n *NotificationAlarm) getResource() (string, string) {
	resourceType := n.getResourceType()
	resourceMsg := n.getResourceMsg()
	return resourceType, resourceMsg
}

func (n *NotificationAlarm) formatMsg(resourceMsg string) string {
	var msg bytes.Buffer
	t, err := template.ParseFiles("templates/syslog")
	if err != nil {
		panic(err)
	}
	err = t.Execute(&msg, map[string]string{
		"name": n.Name,
		"language": syslogLanguage,
		"severity": n.Severity,
		"msgType": resourceMsg,
		"value": strconv.FormatFloat(n.Value.Value, 'f', -1, 64),
		"metricName": n.MetricName,
		"comparison": n.Comparison,
		"threshold": strconv.FormatFloat(n.Threshold, 'f', -1, 64),
	})
	if err != nil {
		panic(err)
	}
	return msg.String()

}

func (n *NotificationAlarm) toSyslog() {
	n.setSeverity()
	resourceType, resourceMsg := n.getResource()
	formattedMsg := n.formatMsg(resourceMsg)
	if n.Severity == "ALERT" {
		resourceType = "insufficient " + resourceType
		SyslogClient.AlertWithTag(resourceType, formattedMsg)
	} else if n.Severity == "CRITICAL" {
		resourceType = "critical " + resourceType
		SyslogClient.CritWithTag(resourceType, formattedMsg)
	} else if n.Severity == "WARNING" {
		resourceType = "warning " + resourceType
		SyslogClient.WarningWithTag(resourceType, formattedMsg)
	} else if n.Severity == "INFO" {
		resourceType = "info " + resourceType
		SyslogClient.InfoWithTag(resourceType, formattedMsg)
	} else {
		xlog.Error.Printf("Alarm severity=%s can't be recognized", n.Severity)
	}
	xlog.Info.Printf("Syslog data: severity=%s, resourceType=%s, msg=%s", n.Severity, resourceType, formattedMsg)
}
