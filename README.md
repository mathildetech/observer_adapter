# Observer Adapter

---

Observer Adapter is an adapter that converts alert notifications to messages in other formats

## Quick Start

---

The default image is ready-to-go. You just need to set your SYSLOG_ADDR in the environment.
```
$ docker run -d --log-driver=json-file -p 80:8080 --name observer-adapter \
    -e LANGUAGE=CN \
    -e SYSLOG_HOSTNAME=alauda \
    -e SYSLOG_ADDR={syslogHost:syslogPort} \
    observer_adapter:latest
```

## Building

---

Development Version
```
$ docker build --target builder -t observer_adapter:dev .
```

Production Version
```
$ docker build -t observer_adapter .
```

## Environment Variables

---

Some configuration parameters can be changed with enviroment variables.

* `LANGUAGE` set the language used by the message (EN, CN), defalut EN.
* `SYSLOG_ADDR` set the syslog address used by syslog server.
* `SYSLOG_HOSTNAME` set the hostname used by syslog messages, defalut hostname.
* `SYSLOG_FACILITY` set the facility used by syslog messages, defalut 23.
* `SYSLOG_NETWORK` set the network mode used by syslog server (UDP, TCP), default UPD.