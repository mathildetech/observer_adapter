package main

import (
	"observer_adapter/xsyslog"
	"time"
	"fmt"
	"strconv"
)

const (
	SYSLOG_FACILITY = xsyslog.LOG_LOCAL7
	SYSLOG_HOSTNAME = "alauda"
	SYSLOG_NETWORK = "udp"
	SYSLOG_ADDR = "127.0.0.1:514"
)

var (
	SyslogHostname string
	SyslogAddr string
	SyslogNetwork string
	SyslogFacility xsyslog.Priority
	SyslogClient *xsyslog.Writer
)

func CITICFormatter(p xsyslog.Priority, hostname, tag, content string) string {
	timestamp := time.Now().Format("Jan 02 15:04:05")
	msg := fmt.Sprintf("<%d>%s %s %s %s", p, timestamp, hostname, tag, content)
	return msg
}

func New() (*xsyslog.Writer, error) {
	w, err := xsyslog.Dial(SyslogNetwork, SyslogAddr, SyslogFacility, "")
	if err != nil {
		return nil, err
	}
	w.SetHostname(SyslogHostname)
	w.SetFormatter(CITICFormatter)
	return w, nil
}

func init() {
	var err error
	if getenv("SYSLOG_FACILITY", "") == "" {
		SyslogFacility = SYSLOG_FACILITY
	} else {
		temp, err := strconv.Atoi(getenv("SYSLOG_FACILITY", ""))
		if err != nil {
			panic(err)
		}
		SyslogFacility = xsyslog.Priority(temp)
	}
	SyslogHostname = getenv("SYSLOG_HOSTNAME", SYSLOG_HOSTNAME)
	SyslogNetwork = getenv("SYSLOG_NETWORK", SYSLOG_NETWORK)
	SyslogAddr = getenv("SYSLOG_ADDR", SYSLOG_ADDR)
	SyslogClient, err = New()
	if err != nil {
		panic(err)
	}
}