FROM golang:1.10 as builder

WORKDIR $GOPATH/src/observer_adapter
COPY . .

RUN go get -v "github.com/jinzhu/copier" \
 && go get -v "github.com/stretchr/testify/assert"
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
RUN go install -v

RUN mkdir -p /var/log/observer_adapter
EXPOSE 8080

CMD ["/go/bin/observer_adapter"]


FROM alpine:3.6

RUN apk add --update ca-certificates

WORKDIR /observer_adapter
COPY . .
COPY --from=builder /go/src/observer_adapter/main ./main
RUN mkdir -p /var/log/observer_adapter \
 && chmod +x /observer_adapter/main

EXPOSE 8080

CMD ["/observer_adapter/main"]