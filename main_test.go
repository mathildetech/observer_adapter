package main

import (
	// "fmt"
	"bytes"
	"testing"
	"net/http"
	"net/http/httptest"
	"io/ioutil"
	"encoding/json"
	"net"
	"strings"
	"strconv"
	"github.com/jinzhu/copier"
	"github.com/stretchr/testify/assert"
)

var metricData map[string]*NotificationAlarm
var metricResult map[string]string
var host, kubeNamespace, service string
var resourceNameCluster, resourceNameNode, resourceNameService string

const (
	syslogTime = "May 04 13:00:00"
	syslogPriAlert = "<185>"
	syslogPriCritical = "<186>"
	syslogPriWarning = "<188>"
	syslogPriInfo = "<190>"
	syslogPlatform = "alauda"
	resourceClusterName = "k8s_new"
	resourceHostName = "10.0.0.1"
	resourceNamespaceName = "default"
	resourceServiceName = "nginx"
)

func init() {
	resourceNameCluster = strings.Join([]string{"cluster_name", resourceClusterName}, "=")
	host = strings.Join([]string{"host", resourceHostName}, "=")
	resourceNameNode = strings.Join([]string{resourceNameCluster, host}, ",")
	kubeNamespace = strings.Join([]string{"kube_namespace", resourceNamespaceName}, "=")
	service = strings.Join([]string{"service_name", resourceServiceName}, "=")
	resourceNameService = strings.Join([]string{resourceNameCluster, kubeNamespace, service}, ",")
	metricData = make(map[string]*NotificationAlarm)
	baseDataEN := NotificationAlarm{
		Name: "alarm-test",
		Start: "2018-04-28 15:56:43",
		Comparison: "GreaterThan",
		Threshold: 0.9,
		Interval: 60,
		StatsTsdb: "avg",
		StatsDuration: 60,
		MetricUnit: "Percent",
	}
	var baseDataCN, nodeBaseDataEN, nodeBaseDataCN NotificationAlarm
	var nodeAlarmEN, nodeAlarmCN NotificationAlarm
	var nodeNormalEN, nodeNormalCN NotificationAlarm
	var nodeInsufficientEN, nodeInsufficientCN NotificationAlarm
	var clusterAlarmEN, clusterAlarmCN NotificationAlarm
	var serviceAlarmEN, serviceAlarmCN NotificationAlarm

	copier.Copy(&baseDataCN, &baseDataEN)
	baseDataCN.Comparison = "大于"

	copier.Copy(&nodeBaseDataEN, &baseDataEN)
	nodeBaseDataEN.MetricName = "node.cpu.utilization"
	nodeBaseDataEN.MetricNameDis = "Node CPU usage"
	nodeBaseDataEN.ResourceName = resourceNameNode
	copier.Copy(&nodeBaseDataCN, &baseDataCN)
	nodeBaseDataCN.MetricName = "node.cpu.utilization"
	nodeBaseDataCN.MetricNameDis = "节点CPU使用率"
	nodeBaseDataCN.ResourceName = resourceNameNode

	copier.Copy(&nodeAlarmEN, &nodeBaseDataEN)
	nodeAlarmEN.Status = "ALARM"
	nodeAlarmEN.Value = JsonFloat64{true, true,0.91}
	metricData["nodeAlarmEN"] = &nodeAlarmEN
	copier.Copy(&nodeAlarmCN, &nodeBaseDataCN)
	nodeAlarmCN.Status = "警报"
	nodeAlarmCN.Value = JsonFloat64{true, true,0.91}
	metricData["nodeAlarmCN"] = &nodeAlarmCN

	copier.Copy(&nodeNormalEN, &nodeBaseDataEN)
	nodeNormalEN.Status = "OK"
	metricData["nodeNormalEN"] = &nodeNormalEN
	copier.Copy(&nodeNormalCN, &nodeBaseDataCN)
	nodeNormalCN.Status = "正常"
	metricData["nodeNormalCN"] = &nodeNormalCN
	
	copier.Copy(&nodeInsufficientEN, &nodeBaseDataEN)
	nodeInsufficientEN.Status = "INSUFFICIENT_DATA"
	metricData["nodeInsufficientEN"] = &nodeInsufficientEN
	copier.Copy(&nodeInsufficientCN, &nodeBaseDataCN)
	nodeInsufficientCN.Status = "数据不足"
	metricData["nodeInsufficientCN"] = &nodeInsufficientCN

	copier.Copy(&clusterAlarmEN, &baseDataEN)
	clusterAlarmEN.MetricName = "cluster.cpu.utilization"
	clusterAlarmEN.MetricNameDis = "Cluster CPU usage"
	clusterAlarmEN.ResourceName = resourceNameCluster
	clusterAlarmEN.Threshold = 0.95
	clusterAlarmEN.Status = "WARNING_ALARM"
	clusterAlarmEN.Value = JsonFloat64{true, true, 0.99}
	metricData["clusterAlarmEN"] = &clusterAlarmEN
	copier.Copy(&clusterAlarmCN, &baseDataCN)
	clusterAlarmCN.MetricName = "cluster.cpu.utilization"
	clusterAlarmCN.MetricNameDis = "集群CPU使用率"
	clusterAlarmCN.ResourceName = resourceNameCluster
	clusterAlarmCN.Threshold = 0.95
	clusterAlarmCN.Status = "警告警报"
	clusterAlarmCN.Value = JsonFloat64{true, true, 0.99}
	metricData["clusterAlarmCN"] = &clusterAlarmCN

	copier.Copy(&serviceAlarmEN, &baseDataEN)
	serviceAlarmEN.MetricName = "service.cpu.utilization"
	serviceAlarmEN.MetricNameDis = "Service CPU usage"
	serviceAlarmEN.ResourceName = resourceNameService
	serviceAlarmEN.Threshold = 0.95
	serviceAlarmEN.Status = "WARNING_ALARM"
	serviceAlarmEN.Value = JsonFloat64{true, true, 0.99}
	metricData["serviceAlarmEN"] = &serviceAlarmEN
	copier.Copy(&serviceAlarmCN, &baseDataCN)
	serviceAlarmCN.MetricName = "service.cpu.utilization"
	serviceAlarmCN.MetricNameDis = "服务CPU使用率"
	serviceAlarmCN.ResourceName = resourceNameService
	serviceAlarmCN.Threshold = 0.95
	serviceAlarmCN.Status = "警告警报"
	serviceAlarmCN.Value = JsonFloat64{true, true, 0.99}
	metricData["serviceAlarmCN"] = &serviceAlarmCN

	metricResult = make(map[string]string)
	metricResult["nodeInsufficientEN"] = strings.Join([]string{syslogPriAlert, syslogTime, " ", syslogPlatform, " ", 
		"insufficient NODE cluster_name@k8s_new|host@10.0.0.1 Alert alarm-test: The metric of node.cpu.utilization is insufficient", 
		"\n"}, 
		"")
	metricResult["nodeInsufficientCN"] = strings.Join([]string{syslogPriAlert, syslogTime, " ", syslogPlatform, " ", 
		"insufficient NODE cluster_name@k8s_new|host@10.0.0.1 警报alarm-test: 监控指标node.cpu.utilization数据不足", 
		"\n"}, 
		"")	
	metricResult["nodeAlarmEN"] = strings.Join([]string{syslogPriCritical, syslogTime, " ", syslogPlatform, " ", 
		"critical NODE cluster_name@k8s_new|host@10.0.0.1 Alert alarm-test: The current value of the node.cpu.utilization is 0.91, which is GreaterThan the threshold 0.9", 
		"\n"}, 
		"")
	metricResult["nodeAlarmCN"] = strings.Join([]string{syslogPriCritical, syslogTime, " ", syslogPlatform, " ", 
		"critical NODE cluster_name@k8s_new|host@10.0.0.1 警报alarm-test: 监控指标node.cpu.utilization当前值0.91 大于 阈值0.9", 
		"\n"}, 
		"")
	metricResult["nodeNormalEN"] = strings.Join([]string{syslogPriInfo, syslogTime, " ", syslogPlatform, " ", 
		"info NODE cluster_name@k8s_new|host@10.0.0.1 Alert alarm-test: The metric of node.cpu.utilization returned to normal, the threshold 0.9", 
		"\n"}, 
		"")
	metricResult["nodeNormalCN"] = strings.Join([]string{syslogPriInfo, syslogTime, " ", syslogPlatform, " ", 
		"info NODE cluster_name@k8s_new|host@10.0.0.1 警报alarm-test: 监控指标node.cpu.utilization恢复正常 阈值0.9", 
		"\n"}, 
		"")
	metricResult["clusterAlarmEN"] = strings.Join([]string{syslogPriWarning, syslogTime, " ", syslogPlatform, " ", 
		"warning CLUSTER cluster_name@k8s_new Alert alarm-test: The current value of the cluster.cpu.utilization is 0.99, which is GreaterThan the threshold 0.95", 
		"\n"}, 
		"")
	metricResult["clusterAlarmCN"] = strings.Join([]string{syslogPriWarning, syslogTime, " ", syslogPlatform, " ", 
		"warning CLUSTER cluster_name@k8s_new 警报alarm-test: 监控指标cluster.cpu.utilization当前值0.99 大于 阈值0.95", 
		"\n"}, 
		"")
	metricResult["serviceAlarmEN"] = strings.Join([]string{syslogPriWarning, syslogTime, " ", syslogPlatform, " ", 
		"warning SERVICE cluster_name@k8s_new|kube_namespace@default|service_name@nginx Alert alarm-test: The current value of the service.cpu.utilization is 0.99, which is GreaterThan the threshold 0.95", 
		"\n"}, 
		"")
	metricResult["serviceAlarmCN"] = strings.Join([]string{syslogPriWarning, syslogTime, " ", syslogPlatform, " ", 
		"warning SERVICE cluster_name@k8s_new|kube_namespace@default|service_name@nginx 警报alarm-test: 监控指标service.cpu.utilization当前值0.99 大于 阈值0.95", 
		"\n"}, 
		"")
}

func TestGetEnv(t *testing.T) {
	assert.NotEqual(t, "PATH", getenv("PATH", "PATH"))
	assert.Equal(t, "TEST_KEY", getenv("TEST_KEY", "TEST_KEY"))
}

func TestIsExist(t *testing.T) {
	testData := MapResource{"1": "11", "2": "22",}
	assert.Equal(t, true, testData.isExist([]string{"1", "2"}))
	assert.Equal(t, true, testData.isExist([]string{"1", "2"}))
	assert.Equal(t, false, testData.isExist([]string{"1", "2", "3"}))
}

func TestToResouceMsg(t *testing.T) {
	testData := MapResource{"1": "11", "2": "22",}
	assert.Equal(t, "11", testData.toResourceMsg([]string{"1",}))
}

func TestMapResourceSet(t *testing.T) {
	testData := make(MapResource)
	testData.set(resourceNameCluster)
	expected := MapResource{
		RESOURCE_KEY_CLUSTER: resourceClusterName,
	}
	assert.Equal(t, true, assert.ObjectsAreEqual(expected, testData))

	testData = make(MapResource)
	testData.set(resourceNameNode)
	expected = MapResource{
		RESOURCE_KEY_CLUSTER: resourceClusterName,
		RESOURCE_KEY_HOST: resourceHostName,
	}
	assert.Equal(t, true, assert.ObjectsAreEqual(expected, testData))

	testData = make(MapResource)
	testData.set(resourceNameService)
	expected = MapResource{
		RESOURCE_KEY_CLUSTER: resourceClusterName,
		RESOURCE_KEY_KUBE_NAMESPACE: resourceNamespaceName,
		RESOURCE_KEY_SERVICE: resourceServiceName,
	}
	assert.Equal(t, true, assert.ObjectsAreEqual(expected, testData))
}

func TestRespErrBody(t *testing.T) {
	testData := new(RespErr)
	errCode := 500
	errMsg := "Internal Error"
	testData.Body(errCode, errMsg, errMsg)
	expected := RespErr{
		Error: RespErrMsg{
			Code: errCode, 
			Message: errMsg, 
			Details: errMsg,
		},
	}
	assert.Equal(t, true, assert.ObjectsAreEqual(expected, *testData))
}

func TestRespErrBodyJson(t *testing.T) {
	errCode := 500
	errMsg := "Internal Error"
	expected, _ := json.Marshal(RespErr{
		Error: RespErrMsg{
			Code: errCode, 
			Message: errMsg, 
			Details: errMsg,
		},
	})
	assert.JSONEq(t, string(expected), string(RespErrBody(errCode, errMsg, errMsg)))
}

func TestUnmarshalJSONFloat64(t *testing.T) {
	output := new(JsonFloat64)
	expected := JsonFloat64{
		Valid: true,
		Set: true,
		Value: 0.11,
	}
	testData := []byte(`"0.11"`)
	_ = output.UnmarshalJSON(testData)
	assert.Equal(t, true, assert.ObjectsAreEqual(expected, *output))

	testData = []byte(`null`)
	output = new(JsonFloat64)
	expected = JsonFloat64{
		Valid: false,
		Set: true,
		Value: 0,
	}
	_ = output.UnmarshalJSON(testData)
	assert.Equal(t, true, assert.ObjectsAreEqual(expected, *output))
}

func TestMarshalJSONFloat64(t *testing.T) {
	testData := JsonFloat64{
		Valid: true,
		Set: true,
		Value: 0.11,
	}
	output, _ := testData.MarshalJSON()
	assert.Equal(t, []byte(`"0.11"`), output)

	testData = JsonFloat64{}
	output, _ = testData.MarshalJSON()
	assert.Equal(t, []byte(`null`), output)
}

func TestSetSeverity(t *testing.T) {
	severityMap := map[string]string{
		"ALARM": "CRITICAL",
		"警报": "CRITICAL",
		"INSUFFICIENT_DATA": "ALERT",
		"数据不足": "ALERT",
		"WARNING_ALARM": "WARNING",
		"警告警报": "WARNING",
		"OK": "INFO",
		"正常": "INFO",
	}
	for key, value := range severityMap {
		testData := new(NotificationAlarm)
		testData.Status = key
		testData.setSeverity()
		assert.Equal(t, value, testData.Severity)
	}
	testData := new(NotificationAlarm)
	testData.Status = "ALARM"
	testData.Severity = "minor"
	testData.setSeverity()
	assert.NotEqual(t, "CRITICAL", testData.Severity)
	assert.Equal(t, "WARNING", testData.Severity)
}

func TestGetResource(t *testing.T) {
	testData := new(NotificationAlarm)
	resourceType, resourceMsg := testData.getResource()
	assert.Equal(t, RESOURCE_TYPE_DEFAULT, resourceType)
	assert.Equal(t, "*", resourceMsg)

	testData = metricData["clusterAlarmEN"]
	resourceType, resourceMsg = testData.getResource()
	assert.Equal(t, RESOURCE_TYPE_CLUSTER, resourceType)
	assert.Equal(t, "cluster_name@k8s_new", resourceMsg)

	testData = metricData["nodeAlarmEN"]
	resourceType, resourceMsg = testData.getResource()
	assert.Equal(t, RESOURCE_TYPE_NODE, resourceType)
	assert.Equal(t, "cluster_name@k8s_new|host@10.0.0.1", resourceMsg)

	testData = metricData["serviceAlarmEN"]
	resourceType, resourceMsg = testData.getResource()
	assert.Equal(t, RESOURCE_TYPE_SERVICE, resourceType)
	assert.Equal(t, "cluster_name@k8s_new|kube_namespace@default|service_name@nginx", resourceMsg)
}

func TestToSyslog(t *testing.T) {
	index := 0
	for key, data := range metricData {
		syslogPort := 9000 + index
		syslogAddr := strings.Join([]string{"127.0.0.1", strconv.Itoa(syslogPort)}, ":")
		SyslogClient.SetRAddr(syslogAddr)
		index = index + 1
		go udpServer("", syslogPort, t, metricResult[key])
		if strings.HasSuffix(key, "EN") {
			syslogLanguage = "EN"
		} else {
			syslogLanguage = "CN"
		}
		data.toSyslog()
	}
}

func TestRootHandler(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(rootHandler))
	defer server.Close()
	resp, err := http.Get(server.URL)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	assert.Equal(t, "Hi there, welcome!", string(body))
}

func TestSyslogHandler(t *testing.T) {
	var excepted string
	server := httptest.NewServer(http.HandlerFunc(syslogHandler))
	defer server.Close()
	index := 0
	for _, value := range metricData {
		syslogPort := 9000 + index
		syslogAddr := strings.Join([]string{"127.0.0.1", strconv.Itoa(syslogPort)}, ":")
		SyslogClient.SetRAddr(syslogAddr)
		index = index + 1
		go udpServer("", syslogPort, t, excepted)
		payload := &NotificationPayload{
			Type: "alarm",
			Data: *value,
		}
		jsonStr, _ := json.Marshal(payload)
		req, err := http.NewRequest("POST", server.URL, bytes.NewBuffer(jsonStr))
		if err != nil {
			panic(err)
		}
		req.Header.Set("Content-Type", "application/json")
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()
		assert.Equal(t, http.StatusNoContent, resp.StatusCode)
	}
}

func udpServer(host string, port int, t *testing.T, excepted string) {
	serverAddr, err := net.ResolveUDPAddr("udp", strings.Join([]string{host, strconv.Itoa(port)}, ":"))
	if err != nil {
		panic(err)
	}
	conn, err := net.ListenUDP("udp", serverAddr)
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	recvBuffer := make([]byte, 1024)
	for {
		n, _, err := conn.ReadFromUDP(recvBuffer)
		if err != nil {
			panic(err)
		}
		if excepted != "" {
			assert.Equal(
				t, 
				strings.Split(excepted, "alauda")[1], 
				strings.Split(string(recvBuffer[:n]), "alauda")[1])
			assert.Equal(
				t, 
				strings.Split(excepted, ">")[0], 
				strings.Split(string(recvBuffer[:n]), ">")[0])
		}
		return
	}
}