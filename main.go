package main

import (
	"fmt"
	"observer_adapter/accesslog"
	"observer_adapter/xlog"
	"net/http"
	"encoding/json"
)

var logFile *xlog.LogFile

func init() {
	logPath := &xlog.LogPath{}
	logFile = logPath.InitLog()
	accesslog.SetDefaultFileLogger("/var/log/observer_adapter/access.log")
}

type NotificationPayload struct {
	Type string `json:"type"`
	Data NotificationAlarm `json:"payload"`
}

type RespErrMsg struct {
	Code int `json:"code"`
	Message string `json:"message"`
	Details string `json:"details,omitempty"`
}
type RespErr struct {
	Error RespErrMsg `json:"error"`
}

func (r *RespErr) Body(code int, msg string, details string) {
	r.Error.Code = code
	r.Error.Message = msg
	r.Error.Details = details
}

func RespErrBody(code int, msg string, details string) []byte {
	r := new(RespErr)
	r.Body(code, msg, details)
	body, err := json.Marshal(r)
	if err != nil {
		panic(err)
	}
	return body
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, welcome!")
}

func syslogHandler(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		body := RespErrBody(http.StatusBadRequest, "Please send a request body", "Request body is empty")
		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "application/json")
		w.Write(body)
		xlog.Error.Println(string(body))
		return
	}
	var payload NotificationPayload
	// var data NotificationAlarm
	err := json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		body := RespErrBody(http.StatusBadRequest, "Failed to decode request body", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "application/json")
		w.Write(body)
		xlog.Error.Println(string(body))
		return
	}
	payload.Data.toSyslog()
	w.WriteHeader(http.StatusNoContent)
}

func main() {
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/v1/syslog", syslogHandler)
	xlog.Log.Fatal(http.ListenAndServe(":8080", accesslog.LoggingHandler(http.DefaultServeMux, nil)))
}