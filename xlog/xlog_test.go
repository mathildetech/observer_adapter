package xlog

import (
	"testing"
	"os"
	"path/filepath"
)

func TestLogPathInitLog(t *testing.T) {
	if _, err := os.Stat(XFilePath); os.IsNotExist(err) {
		err = os.MkdirAll(XFilePath, 0755)
		if err != nil {
			panic(err)
		}
	}
	logPath := &LogPath{}
	logPath.InitLog()
	Error.Println("error log testing...")
	Warning.Println("warning log testing...")
	Info.Println("info log testing...")
	Debug.Println("debug log testing...")
	os.Truncate(filepath.Join(XFilePath, XFileNameError), 0)
	os.Truncate(filepath.Join(XFilePath, XFileNameWarning), 0)
	os.Truncate(filepath.Join(XFilePath, XFileNameInfo), 0)
	os.Truncate(filepath.Join(XFilePath, XFileNameDebug), 0)
}