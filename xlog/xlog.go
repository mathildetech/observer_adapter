// Package log support leveled logs

package xlog

import (
	"io"
	"log"
	"os"
	"path/filepath"
)

const (
	XLogFlags = log.Ldate | log.Lmicroseconds | log.Lshortfile
	XLogFileFlags = os.O_CREATE | os.O_WRONLY | os.O_APPEND
	XFileNameDebug = "debug.log"
	XFileNameInfo = "info.log"
	XFileNameWarning = "warn.log"
	XFileNameError = "error.log"

)

var (
	XFilePath string
	Debug *log.Logger
	Info *log.Logger
	Warning *log.Logger
	Error *log.Logger
	Log *log.Logger
)

type LogPath struct {
	Path string
	DebugName string
	InfoName string
	WarningName string
	ErrorName string
}

type LogFile struct {
	debugFile *os.File
	infoFile *os.File
	warningFile *os.File
	errorFile *os.File
}

func init() {
	XFilePath = filepath.FromSlash("/var/log/observer_adapter")
	Debug = NewDebugLog(os.Stderr)
	Info = NewInfoLog(os.Stderr)
	Warning = NewWarningLog(os.Stderr)
	Error = NewErrorLog(os.Stderr)
	Log = log.New(os.Stderr, "LOG ", XLogFlags)
}
		
func (lp *LogPath) logFile() *LogFile{
	if lp.Path == "" {
		lp.Path = XFilePath
	}
	if lp.DebugName == "" {
		lp.DebugName = XFileNameDebug
	}
	if lp.InfoName == "" {
		lp.InfoName = XFileNameInfo
	}
	if lp.WarningName == "" {
		lp.WarningName = XFileNameWarning
	}
	if lp.ErrorName == "" {
		lp.ErrorName = XFileNameError
	}
	debugFile, err := os.OpenFile(filepath.Join(lp.Path, lp.DebugName), XLogFileFlags, 0666)
	if err != nil {
		log.Fatalln("error opening file: ", err)
	}
	infoFile, err := os.OpenFile(filepath.Join(lp.Path, lp.InfoName), XLogFileFlags, 0666)
	if err != nil {
		log.Fatalln("error opening file: ", err)
	}
	warningFile, err := os.OpenFile(filepath.Join(lp.Path, lp.WarningName), XLogFileFlags, 0666)
	if err != nil {
		log.Fatalln("error opening file: ", err)
	}
	errFile, err := os.OpenFile(filepath.Join(lp.Path, lp.ErrorName), XLogFileFlags, 0666)
	if err != nil {
		log.Fatalln("error opening file: ", err)
	}
	return &LogFile{debugFile, infoFile, warningFile, errFile}
}

func (lp *LogPath) InitLog() *LogFile {
	logFile := lp.logFile()
	logFile.InitLog()
	return logFile
}

func (lf *LogFile) InitLog () {
	Debug = NewDebugLog(lf.debugFile)
	Info = NewInfoLog(io.MultiWriter(lf.infoFile, lf.debugFile))
	Warning = NewWarningLog(io.MultiWriter(lf.warningFile, lf.infoFile, lf.debugFile))
	Error = NewErrorLog(io.MultiWriter(lf.errorFile, lf.warningFile, lf.infoFile, lf.debugFile))
}

func NewDebugLog(out io.Writer) *log.Logger {
	return log.New(out, "DEBUG   ", XLogFlags)
}

func NewInfoLog(out io.Writer) *log.Logger {
	return log.New(out, "INFO    ", XLogFlags)
}

func NewWarningLog(out io.Writer) *log.Logger {
	return log.New(out, "WARNING ", XLogFlags)
}

func NewErrorLog(out io.Writer) *log.Logger {
	return log.New(out, "ERROR   ", XLogFlags)
}

func (lf *LogFile) InfoWriter() io.Writer {
	return lf.infoFile
}

func NewFileLogger(file string, prefix string, flag int) *log.Logger{
	fileWriter, err := os.OpenFile(file, XLogFileFlags, 0666)
	if err != nil {
		log.Fatalln("error opening file: ", err)
	}
	return log.New(fileWriter, prefix, flag)
}

	