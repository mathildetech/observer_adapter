package xsyslog

import (
	"sync"
	"strings"
)

// A Writer is a connection to a syslog server.
type Writer struct {
	priority Priority
	tag      string
	hostname string
	network  string
	raddr    string

	formatter Formatter

	mu   sync.RWMutex // guards conn
	conn serverConn
}

// SetFramer changes the framer function for subsequent messages.
func (w *Writer) SetFormatter(f Formatter) {
	w.formatter = f
}

// SetHostname changes the hostname for subsequent messages if needed.
func (w *Writer) SetHostname(hostname string) {
	w.hostname = hostname
}

// SetRAddr changes the raddr for subsequent messages if needed.
func (w *Writer) SetRAddr(raddr string) {
	w.Close()
	w.raddr = raddr
}

// getConn provides access to the internal conn, protected by a mutex. The
// conn is threadsafe, so it can be used while unlocked, but we want to avoid
// race conditions on grabbing a reference to it.
func (w *Writer) getConn() serverConn {
	w.mu.RLock()
	conn := w.conn
	w.mu.RUnlock()
	return conn
}

// setConn updates the internal conn, protected by a mutex.
func (w *Writer) setConn(c serverConn) {
	w.mu.Lock()
	w.conn = c
	w.mu.Unlock()
}

// connect makes a connection to the syslog server.
func (w *Writer) connect() (serverConn, error) {
	conn := w.getConn()
	if conn != nil {
		// ignore err from close, it makes sense to continue anyway
		conn.close()
		w.setConn(nil)
	}
	conn, hostname, err := w.dial()
	if err == nil {
		w.setConn(conn)
		w.hostname = hostname
		return conn, nil
	} else {
		return nil, err
	}
}

// Close closes a connection to the syslog daemon.
func (w *Writer) Close() error {
	conn := w.getConn()
	if conn != nil {
		err := conn.close()
		w.setConn(nil)
		return err
	}
	return nil
}

// write generates and writes a syslog formatted string. It formats the
// message based on the current Formatter and Framer.
func (w *Writer) write(conn serverConn, p Priority, tag, msg string) (int, error) {
	// ensure it ends in a \n
	if !strings.HasSuffix(msg, "\n") {
		msg += "\n"
	}
	if tag == "" {
		tag = w.tag
	}
	err := conn.writeString(w.formatter, p, w.hostname, tag, msg)
	if err != nil {
		return 0, err
	}
	// Note: return the length of the input, not the number of
	// bytes printed by Fprintf, because this must behave like
	// an io.Writer.
	return len(msg), nil
}

// writeAndRetryWithPriority differs from writeAndRetry in that it allows setting
// of both the facility and the severity.
func (w *Writer) writeAndRetryWithPriority(p Priority, tag, msg string) (int, error) {
	conn := w.getConn()
	if conn != nil {
		if n, err := w.write(conn, p, tag, msg); err == nil {
			return n, err
		}
	}
	var err error
	if conn, err = w.connect(); err != nil {
		return 0, err
	}
	return w.write(conn, p, tag, msg)
}

// writeAndRetry takes a severity and the string to write. Any facility passed to
// it as part of the severity Priority will be ignored.
func (w *Writer) writeAndRetry(severity Priority, tag, msg string) (int, error) {
	pr := (w.priority & facilityMask) | (severity & severityMask)
	return w.writeAndRetryWithPriority(pr, tag, msg)
}

// WriteWithPriority sends a log message with a custom priority.
func (w *Writer) WriteWithPriority(p Priority, tag, msg string) (int, error) {
	return w.writeAndRetryWithPriority(p, tag, msg)
}

// Emerg logs a message with severity LOG_EMERG, ignoring the severity
// passed to New.
func (w *Writer) Emerg(msg string) error {
	_, err := w.writeAndRetry(LOG_EMERG, "", msg)
	return err
}

func (w *Writer) EmergWithTag(tag, msg string) error {
	_, err := w.writeAndRetry(LOG_EMERG, tag, msg)
	return err
}

// Alert logs a message with severity LOG_ALERT, ignoring the severity
// passed to New.
func (w *Writer) Alert(msg string) error {
	_, err := w.writeAndRetry(LOG_ALERT, "", msg)
	return err
}

func (w *Writer) AlertWithTag(tag, msg string) error {
	_, err := w.writeAndRetry(LOG_ALERT, tag, msg)
	return err
}

// Crit logs a message with severity LOG_CRIT, ignoring the severity
// passed to New.
func (w *Writer) Crit(msg string) error {
	_, err := w.writeAndRetry(LOG_CRIT, "", msg)
	return err
}

func (w *Writer) CritWithTag(tag, msg string) error {
	_, err := w.writeAndRetry(LOG_CRIT, tag, msg)
	return err
}

// Err logs a message with severity LOG_ERR, ignoring the severity
// passed to New.
func (w *Writer) Err(msg string) error {
	_, err := w.writeAndRetry(LOG_ERR, "", msg)
	return err
}

func (w *Writer) ErrWithTag(tag, msg string) error {
	_, err := w.writeAndRetry(LOG_ERR, tag, msg)
	return err
}

// Warning logs a message with severity LOG_WARNING, ignoring the
// severity passed to New.
func (w *Writer) Warning(msg string) error {
	_, err := w.writeAndRetry(LOG_WARNING, "", msg)
	return err
}

func (w *Writer) WarningWithTag(tag, msg string) error {
	_, err := w.writeAndRetry(LOG_WARNING, tag, msg)
	return err
}

// Notice logs a message with severity LOG_NOTICE, ignoring the
// severity passed to New.
func (w *Writer) Notice(msg string) error {
	_, err := w.writeAndRetry(LOG_NOTICE, "", msg)
	return err
}

func (w *Writer) NoticeWithTag(tag, msg string) error {
	_, err := w.writeAndRetry(LOG_NOTICE, tag, msg)
	return err
}

// Info logs a message with severity LOG_INFO, ignoring the severity
// passed to New.
func (w *Writer) Info(msg string) error {
	_, err := w.writeAndRetry(LOG_INFO, "", msg)
	return err
}

func (w *Writer) InfoWithTag(tag, msg string) error {
	_, err := w.writeAndRetry(LOG_INFO, tag, msg)
	return err
}

// Debug logs a message with severity LOG_DEBUG, ignoring the severity
// passed to New.
func (w *Writer) Debug(msg string) error {
	_, err := w.writeAndRetry(LOG_DEBUG, "", msg)
	return err
}

func (w *Writer) DebugWithTag(tag, msg string) error {
	_, err := w.writeAndRetry(LOG_DEBUG, tag, msg)
	return err
}

// Write sends a log message to the syslog daemon using the default priority
// passed into `xsyslog.New` or the `xsyslog.Dial*` functions.
func (w *Writer) Write(b []byte) (int, error) {
	return w.writeAndRetry(w.priority, "", string(b))
}