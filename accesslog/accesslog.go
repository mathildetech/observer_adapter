package accesslog

import (
	"time"
	"net/http"
	"strings"
	"log"
	"bytes"
	"fmt"
	"os"
	"observer_adapter/xlog"
)

var defaultFileLogger *log.Logger

type LogRecord struct {
	Time time.Time
	Ip, Method, Uri, Protocol, Username, Host, UserAgent string
	Status int
	ResponseSize int64
	RequestSize int64
	ElapsedTime time.Duration
	RequestHeader http.Header
	CustomRecords map[string]string
}

type Logger interface {
	Log(record LogRecord)
}

func init() {
	defaultFileLogger = log.New(os.Stdout, "", 0)
}

type defaultLogger struct {}

func (l defaultLogger) Log(r LogRecord) {
	buff := new(bytes.Buffer)
	fmt.Fprintf(
		buff, 
		"%s - %s [%s] \"%s %s %s\" %d %d %d \"-\" \"%s\" \"-\" \"%s\" rt=%f", 
		r.Ip, r.Username, r.Time.Format("02/Jan/2006:15:04:05 -0700"), r.Method, r.Uri, r.Protocol, 
		r.Status, r.RequestSize, r.ResponseSize, 
		r.UserAgent, r.Host, float64(r.ElapsedTime)/float64(time.Second))
	defaultFileLogger.Println(buff.String())
}

type loggingHandler struct {
	handler http.Handler
	logger Logger
}

func (h *loggingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ip := strings.Split(r.RemoteAddr, ":")[0]

	username := "-"
	if name := r.URL.User.Username(); name != "" {
		username = name
	}

	startTime := time.Now()
	lw := &loggingResponseWriter{
		ResponseWriter: w,
		logRecord: LogRecord{
			Time: startTime.UTC(),
			Ip: ip,
			Method: r.Method,
			Uri: r.RequestURI,
			UserAgent: r.UserAgent(),
			Username: username,
			Protocol: r.Proto,
			Host: r.Host,
			Status: 0,
			ResponseSize: 0,
			RequestSize: r.ContentLength,
			ElapsedTime: time.Duration(0),
			RequestHeader: r.Header,
		},
	}
	
	h.handler.ServeHTTP(lw, r)
	finishTime := time.Now()

	lw.logRecord.Time = finishTime
	lw.logRecord.ElapsedTime = finishTime.Sub(startTime)

	h.logger.Log(lw.logRecord)
}

func LoggingHandler(handler http.Handler, logger Logger) http.Handler {
	if logger == nil {
		logger = defaultLogger{}
	}
	return &loggingHandler{
		handler: handler,
		logger: logger,
	}
}

// Interface ResponseWriter
// http ResponseWriter may not be used after the Handler.ServeHTTP method has returned.
type loggingResponseWriter struct {
	http.ResponseWriter
	logRecord LogRecord
}

func (w *loggingResponseWriter) Header() http.Header {
	return w.ResponseWriter.Header()
}

func (w *loggingResponseWriter) Write(p []byte) (int, error) {
	if w.logRecord.Status == 0 {
		w.logRecord.Status = http.StatusOK
	}
	n, err := w.ResponseWriter.Write(p)
	w.logRecord.ResponseSize += int64(n)
	return n, err
}

func (w *loggingResponseWriter) WriteHeader(statusCode int) {
	w.logRecord.Status = statusCode
	w.ResponseWriter.WriteHeader(statusCode)
}

func (w *loggingResponseWriter) Flush() {
	if f, ok := w.ResponseWriter.(http.Flusher); ok {
		f.Flush()
	}
}

func (w *loggingResponseWriter) SetCustomLogRecord(key, value string) {
	if w.logRecord.CustomRecords == nil {
		w.logRecord.CustomRecords = map[string]string{}
	}
	w.logRecord.CustomRecords[key] = value
}

func SetDefaultFileLogger(file string) {
	defaultFileLogger = xlog.NewFileLogger(file, "", 0)
}